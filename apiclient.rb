#!/usr/bin/env ruby


require 'docker'

class DockerWraper
  def initialize(ip,port)
    Docker.url = "tcp://#{ip}:#{port}"
  end
  
  def create_container(image,name,port,cmd=nil,link=nil)
    container = Docker::Container.create('image'=>image,'tty'=>true, 'name'=>name,'OpenStdin'=>true, 'StdinOnce'=>true, "ExposedPorts"=>{"#{port}/tcp"=>{}})
    start_container(container,link,port)
    save_image(container,name)
    container_cmd(container,cmd)
  end
  
  def container_cmd(container,cmd)
    unless cmd.nil?
      post_command(container, cmd)
    end
  
  end

  def start_container(container,link,port)

    val = {}
    ps = {"PortBindings"=>{"#{port}/tcp"=>[{"HostPort"=>"#{port}"}]}}
    ls = {"Links"=>["#{link}"]} 

    if link.nil?
      val.merge!(ps)
    elsif port.nil?
      val.merge!(ls)
    elsif !link.nil? && !port.nil?
      val.merge!(ps)
      val.merge!(ls)
    end

    container.start(val)

  end

  def save_image(container,name)
    stamp = DateTime.now.strftime('%Y%m%d%H%M%S')
    container.commit('repo'=>'local','tag'=>"#{name}_#{stamp}")
  end

  def post_command(container,cmd)
    command = ["bash", "-c", "#{cmd}"]  
    container.exec(command, tty: true)  
  end

  def kill_containers
    Docker::Container.all.each do |dc|
      puts "stoping: #{dc.info['Names'].first}"
      dc.kill!
      dc.remove
    end
  end

end