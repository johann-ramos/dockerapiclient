#!/usr/bin/env ruby

require_relative 'apiclient'

def usage(s)
  $stderr.puts(s)
  $stderr.puts "Usage: #{File.basename($0)}: [opts]\n\n[-a <action (create|kill) >]\n[-i <docker_image>]\n[-n <docker_name>]\n[-p <port>]\n[-c <command>]\n[-l <docker_link>]\n" 
  exit(2)
end

def params_options(containers)
  loop do
    case ARGV[0]
      when '-a' then  ARGV.shift; containers.store(:action,ARGV.shift)
      when '-i' then  ARGV.shift; containers.store(:image,ARGV.shift)
      when '-n' then  ARGV.shift; containers.store(:name,ARGV.shift)
      when '-p' then  ARGV.shift; containers.store(:port,ARGV.shift)
      when '-c' then  ARGV.shift; containers.store(:cmd,ARGV.shift)
      when '-l' then  ARGV.shift; containers.store(:link,ARGV.shift)
      when /^-/ then  usage("Unknown option: #{ARGV[0].inspect}")
      else
        break
    end
  end
end

def main
  logfile = nil
  containers = {}

  params_options(containers)
  
  dc1 = DockerWraper.new("172.17.100.175","2376")
  puts containers[:action]
  if containers[:action] == 'create'
    if containers[:action] != nil && containers[:image] != nil && containers[:name] != nil
      puts "logfile: #{logfile.inspect} args: #{ARGV.inspect}"
      puts "containers: #{containers}"
      dc1.create_container(containers[:image],containers[:name],containers[:port],containers[:cmd],containers[:link])
    end
  elsif containers[:action] == 'kill'
    dc1.kill_containers
  else
    puts 'Unknown action'
  end

end

main